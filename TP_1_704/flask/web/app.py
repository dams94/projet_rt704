from flask import Flask, render_template, request
 
app = Flask(__name__)
 
 
@app.route('/')
def hello_whale():
    return render_template("hello.html")

@app.route('/resultat',methods = ['POST'])
def resultat():
    result = request.form
    n = result['prenom']
    p = result['nom']
    return render_template("resultat.html", prenom=n, nom=p)
 
if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0', port=5000)


import pika


def create_queue(queue):
    connection = pika.BlockingConnection(pika.ConnectionParameters(host='localhost'))
    channel = connection.channel()
    channel.queue_declare(queue=queue)
    connection.close()

    print("___queue creted")
    return("---you are created a queue")

def write_queue(queue,message):

    connection = pika.BlockingConnection(pika.ConnectionParameters(host='localhost'))
    channel = connection.channel()
    channel.basic_publish(exchange='', routing_key=queue, body=message)
    connection.close()
    return("you sent a message to {}" .format(queue))

def read_queue(queue):

    connection = pika.BlockingConnection(pika.ConnectionParameters(host='localhost'))
    channel = connection.channel()

    method_frame, header_frame, body=channel.basic_get(queue)
    if method_frame:
        print(method_frame, body)
        channel.basic_ack(method_frame.delivery_tag)

        return body
    
    else:

      print("nothing read")




if __name__=='__main__':
    create_queue("Test1234")
    write_queue("Test1234","message pour faire le test")
    #read_queue("Test1")
